#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <dsound.h>
#include <string>
#include <audiopolicy.h>

#pragma once
#pragma comment(lib, "dsound.lib")

namespace SLFSampleSequencer {

	using namespace std;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// Summary for mainForm
	/// </summary>
	public ref class mainForm : public System::Windows::Forms::Form
	{
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad1;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad2;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad4;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad3;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad7;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad8;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad6;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad5;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad13;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad14;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad16;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad15;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad11;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad12;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad10;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  pad9;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialogAudio;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  btnClear;

	private: System::Windows::Forms::Button^  btnSequence;
	private: System::Windows::Forms::Button^  btnSeqPrev;
	private: System::Windows::Forms::Button^  btnSeqNext;
	private: System::Windows::Forms::Button^  btnMet;
	
	int		Tempo, timerInterval, seqPosition, i, LoopLength, Volume;
	String	^audioFilePad1, ^audioFilePad2, ^audioFilePad3, ^audioFilePad4, ^audioFilePad5, ^audioFilePad6,
			^audioFilePad7, ^audioFilePad8, ^audioFilePad9, ^audioFilePad10, ^audioFilePad11, ^audioFilePad12,
			^audioFilePad13, ^audioFilePad14, ^audioFilePad15, ^audioFilePad16, ^sampleName, ^dots, ^hits;
	bool	audioOn, metronome, seqMode,
			seqEdit1, seqEdit2, seqEdit3, seqEdit4, seqEdit5, seqEdit6, seqEdit7, seqEdit8,
			seqEdit9, seqEdit10, seqEdit11, seqEdit12, seqEdit13, seqEdit14, seqEdit15, seqEdit16;
	System::Media::SoundPlayer	^pad1Player, ^pad2Player, ^pad3Player, ^pad4Player, ^pad5Player, ^pad6Player,
								^pad7Player, ^pad8Player, ^pad9Player, ^pad10Player, ^pad11Player, ^pad12Player,
								^pad13Player, ^pad14Player, ^pad15Player, ^pad16Player, ^metronomePlayer;
	static cli::array<bool>
				^playPad1 = gcnew array<bool>(17),
				^playPad2 = gcnew array<bool>(17),
				^playPad3 = gcnew array<bool>(17), 
				^playPad4 = gcnew array<bool>(17), 
				^playPad5 = gcnew array<bool>(17), 
				^playPad6 = gcnew array<bool>(17), 
				^playPad7 = gcnew array<bool>(17), 
				^playPad8 = gcnew array<bool>(17),
				^playPad9 = gcnew array<bool>(17), 
				^playPad10 = gcnew array<bool>(17), 
				^playPad11 = gcnew array<bool>(17), 
				^playPad12 = gcnew array<bool>(17), 
				^playPad13 = gcnew array<bool>(17), 
				^playPad14 = gcnew array<bool>(17), 
				^playPad15 = gcnew array<bool>(17), 
				^playPad16 = gcnew array<bool>(17);
	private: System::Windows::Forms::ToolStripMenuItem^  toolsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  metronomeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  sequenceModeToolStripMenuItem;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TrackBar^  volumeBar;

	private: System::Windows::Forms::TextBox^  txtVolume;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::ListBox^  lboSample;
	private: System::Windows::Forms::Button^  btnLoop4;
	private: System::Windows::Forms::Button^  btnLoop8;
	private: System::Windows::Forms::Button^  btnLoop16;
	private: System::Windows::Forms::Button^  btnLoop12;
	private: System::Windows::Forms::Label^  label5;

	private: System::Windows::Forms::Label^  label3;
	
	DWORD outputVolume;
	HWAVEOUT hwo;
	
	public:
		mainForm(void)
		{
			InitializeComponent();
			audioOn = false;
			seqPosition = 1;
			seqMode = false;
			timerTempo->Enabled = false;
			metronome = false;
			metronomePlayer = gcnew System::Media::SoundPlayer("C:\\Users\\Kelsall\\Documents\\Test Samples\\MetroLow.wav");
			for(i = 1; i > 17; i++)
			{
				playPad1[i] = false;
				playPad2[i] = false;
				playPad3[i] = false;
				playPad4[i] = false;
				playPad5[i] = false;
				playPad6[i] = false;
				playPad7[i] = false;
				playPad8[i] = false;
				playPad9[i] = false;
				playPad10[i] = false;
				playPad11[i] = false;
				playPad12[i] = false;
				playPad13[i] = false;
				playPad14[i] = false;
				playPad15[i] = false;
				playPad16[i] = false;
			}
			LoopLength = 16;
			setLoopLength(LoopLength);
			dots = "...";
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~mainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  newToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveAsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: Microsoft::VisualBasic::PowerPacks::ShapeContainer^  shapeContainer1;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq9;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq10;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq12;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq11;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq15;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq16;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq14;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq13;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq5;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq6;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq8;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq7;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq3;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq4;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq2;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  seq1;
	private: System::Windows::Forms::TrackBar^  tempoBar;
	private: System::Windows::Forms::Label^  lblTempo;
	private: System::Windows::Forms::TextBox^  txtBoxTempo;
	private: System::Windows::Forms::Button^  btnPlay;
	private: System::Windows::Forms::Button^  btnPause;
	private: System::Windows::Forms::Button^  btnStop;
	private: System::Windows::Forms::Timer^  timerTempo;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->newToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveAsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->metronomeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->sequenceModeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->shapeContainer1 = (gcnew Microsoft::VisualBasic::PowerPacks::ShapeContainer());
			this->pad1 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad2 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad4 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad3 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad7 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad8 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad6 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad5 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad13 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad14 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad16 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad15 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad11 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad12 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad10 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->pad9 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq9 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq10 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq12 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq11 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq15 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq16 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq14 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq13 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq5 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq6 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq8 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq7 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq3 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq4 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq2 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->seq1 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->tempoBar = (gcnew System::Windows::Forms::TrackBar());
			this->lblTempo = (gcnew System::Windows::Forms::Label());
			this->txtBoxTempo = (gcnew System::Windows::Forms::TextBox());
			this->btnPlay = (gcnew System::Windows::Forms::Button());
			this->btnPause = (gcnew System::Windows::Forms::Button());
			this->btnStop = (gcnew System::Windows::Forms::Button());
			this->timerTempo = (gcnew System::Windows::Forms::Timer(this->components));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->openFileDialogAudio = (gcnew System::Windows::Forms::OpenFileDialog());
			this->btnClear = (gcnew System::Windows::Forms::Button());
			this->btnSequence = (gcnew System::Windows::Forms::Button());
			this->btnSeqPrev = (gcnew System::Windows::Forms::Button());
			this->btnSeqNext = (gcnew System::Windows::Forms::Button());
			this->btnMet = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->volumeBar = (gcnew System::Windows::Forms::TrackBar());
			this->txtVolume = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->lboSample = (gcnew System::Windows::Forms::ListBox());
			this->btnLoop4 = (gcnew System::Windows::Forms::Button());
			this->btnLoop8 = (gcnew System::Windows::Forms::Button());
			this->btnLoop16 = (gcnew System::Windows::Forms::Button());
			this->btnLoop12 = (gcnew System::Windows::Forms::Button());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->tempoBar))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->volumeBar))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->fileToolStripMenuItem, 
				this->toolsToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(570, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {this->newToolStripMenuItem, 
				this->openToolStripMenuItem, this->saveToolStripMenuItem, this->saveAsToolStripMenuItem, this->exitToolStripMenuItem});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// newToolStripMenuItem
			// 
			this->newToolStripMenuItem->Name = L"newToolStripMenuItem";
			this->newToolStripMenuItem->Size = System::Drawing::Size(114, 22);
			this->newToolStripMenuItem->Text = L"New";
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->Size = System::Drawing::Size(114, 22);
			this->openToolStripMenuItem->Text = L"Open";
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->Size = System::Drawing::Size(114, 22);
			this->saveToolStripMenuItem->Text = L"Save";
			// 
			// saveAsToolStripMenuItem
			// 
			this->saveAsToolStripMenuItem->Name = L"saveAsToolStripMenuItem";
			this->saveAsToolStripMenuItem->Size = System::Drawing::Size(114, 22);
			this->saveAsToolStripMenuItem->Text = L"Save As";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(114, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &mainForm::exitToolStripMenuItem_Click);
			// 
			// toolsToolStripMenuItem
			// 
			this->toolsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->metronomeToolStripMenuItem, 
				this->sequenceModeToolStripMenuItem});
			this->toolsToolStripMenuItem->Name = L"toolsToolStripMenuItem";
			this->toolsToolStripMenuItem->Size = System::Drawing::Size(48, 20);
			this->toolsToolStripMenuItem->Text = L"Tools";
			// 
			// metronomeToolStripMenuItem
			// 
			this->metronomeToolStripMenuItem->Name = L"metronomeToolStripMenuItem";
			this->metronomeToolStripMenuItem->Size = System::Drawing::Size(159, 22);
			this->metronomeToolStripMenuItem->Text = L"Metronome";
			this->metronomeToolStripMenuItem->Click += gcnew System::EventHandler(this, &mainForm::metronomeToolStripMenuItem_Click);
			// 
			// sequenceModeToolStripMenuItem
			// 
			this->sequenceModeToolStripMenuItem->Name = L"sequenceModeToolStripMenuItem";
			this->sequenceModeToolStripMenuItem->Size = System::Drawing::Size(159, 22);
			this->sequenceModeToolStripMenuItem->Text = L"Sequence Mode";
			this->sequenceModeToolStripMenuItem->Click += gcnew System::EventHandler(this, &mainForm::sequenceModeToolStripMenuItem_Click);
			// 
			// shapeContainer1
			// 
			this->shapeContainer1->Location = System::Drawing::Point(0, 0);
			this->shapeContainer1->Margin = System::Windows::Forms::Padding(0);
			this->shapeContainer1->Name = L"shapeContainer1";
			this->shapeContainer1->Shapes->AddRange(gcnew cli::array< Microsoft::VisualBasic::PowerPacks::Shape^  >(32) {this->pad1, this->pad2, 
				this->pad4, this->pad3, this->pad7, this->pad8, this->pad6, this->pad5, this->pad13, this->pad14, this->pad16, this->pad15, this->pad11, 
				this->pad12, this->pad10, this->pad9, this->seq9, this->seq10, this->seq12, this->seq11, this->seq15, this->seq16, this->seq14, 
				this->seq13, this->seq5, this->seq6, this->seq8, this->seq7, this->seq3, this->seq4, this->seq2, this->seq1});
			this->shapeContainer1->Size = System::Drawing::Size(570, 426);
			this->shapeContainer1->TabIndex = 17;
			this->shapeContainer1->TabStop = false;
			// 
			// pad1
			// 
			this->pad1->BackColor = System::Drawing::SystemColors::Control;
			this->pad1->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad1->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad1->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad1->Location = System::Drawing::Point(12, 60);
			this->pad1->Name = L"pad1";
			this->pad1->Size = System::Drawing::Size(75, 75);
			this->pad1->Click += gcnew System::EventHandler(this, &mainForm::pad1_Click);
			// 
			// pad2
			// 
			this->pad2->BackColor = System::Drawing::SystemColors::Control;
			this->pad2->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad2->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad2->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad2->Location = System::Drawing::Point(93, 60);
			this->pad2->Name = L"pad2";
			this->pad2->Size = System::Drawing::Size(75, 75);
			this->pad2->Click += gcnew System::EventHandler(this, &mainForm::pad2_Click);
			// 
			// pad4
			// 
			this->pad4->BackColor = System::Drawing::SystemColors::Control;
			this->pad4->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad4->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad4->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad4->Location = System::Drawing::Point(255, 60);
			this->pad4->Name = L"pad4";
			this->pad4->Size = System::Drawing::Size(75, 75);
			this->pad4->Click += gcnew System::EventHandler(this, &mainForm::pad4_Click);
			// 
			// pad3
			// 
			this->pad3->BackColor = System::Drawing::SystemColors::Control;
			this->pad3->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad3->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad3->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad3->Location = System::Drawing::Point(174, 60);
			this->pad3->Name = L"pad3";
			this->pad3->Size = System::Drawing::Size(75, 75);
			this->pad3->Click += gcnew System::EventHandler(this, &mainForm::pad3_Click);
			// 
			// pad7
			// 
			this->pad7->BackColor = System::Drawing::SystemColors::Control;
			this->pad7->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad7->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad7->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad7->Location = System::Drawing::Point(174, 141);
			this->pad7->Name = L"pad7";
			this->pad7->Size = System::Drawing::Size(75, 75);
			this->pad7->Click += gcnew System::EventHandler(this, &mainForm::pad7_Click);
			// 
			// pad8
			// 
			this->pad8->BackColor = System::Drawing::SystemColors::Control;
			this->pad8->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad8->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad8->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad8->Location = System::Drawing::Point(255, 141);
			this->pad8->Name = L"pad8";
			this->pad8->Size = System::Drawing::Size(75, 75);
			this->pad8->Click += gcnew System::EventHandler(this, &mainForm::pad8_Click);
			// 
			// pad6
			// 
			this->pad6->BackColor = System::Drawing::SystemColors::Control;
			this->pad6->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad6->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad6->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad6->Location = System::Drawing::Point(93, 141);
			this->pad6->Name = L"pad6";
			this->pad6->Size = System::Drawing::Size(75, 75);
			this->pad6->Click += gcnew System::EventHandler(this, &mainForm::pad6_Click);
			// 
			// pad5
			// 
			this->pad5->BackColor = System::Drawing::SystemColors::Control;
			this->pad5->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad5->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad5->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad5->Location = System::Drawing::Point(12, 141);
			this->pad5->Name = L"pad5";
			this->pad5->Size = System::Drawing::Size(75, 75);
			this->pad5->Click += gcnew System::EventHandler(this, &mainForm::pad5_Click);
			// 
			// pad13
			// 
			this->pad13->BackColor = System::Drawing::SystemColors::Control;
			this->pad13->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad13->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad13->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad13->Location = System::Drawing::Point(12, 303);
			this->pad13->Name = L"pad13";
			this->pad13->Size = System::Drawing::Size(75, 75);
			this->pad13->Click += gcnew System::EventHandler(this, &mainForm::pad13_Click);
			// 
			// pad14
			// 
			this->pad14->BackColor = System::Drawing::SystemColors::Control;
			this->pad14->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad14->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad14->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad14->Location = System::Drawing::Point(93, 303);
			this->pad14->Name = L"pad14";
			this->pad14->Size = System::Drawing::Size(75, 75);
			this->pad14->Click += gcnew System::EventHandler(this, &mainForm::pad14_Click);
			// 
			// pad16
			// 
			this->pad16->BackColor = System::Drawing::SystemColors::Control;
			this->pad16->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad16->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad16->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad16->Location = System::Drawing::Point(255, 303);
			this->pad16->Name = L"pad16";
			this->pad16->Size = System::Drawing::Size(75, 75);
			this->pad16->Click += gcnew System::EventHandler(this, &mainForm::pad16_Click);
			// 
			// pad15
			// 
			this->pad15->BackColor = System::Drawing::SystemColors::Control;
			this->pad15->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad15->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad15->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad15->Location = System::Drawing::Point(174, 303);
			this->pad15->Name = L"pad15";
			this->pad15->Size = System::Drawing::Size(75, 75);
			this->pad15->Click += gcnew System::EventHandler(this, &mainForm::pad15_Click);
			// 
			// pad11
			// 
			this->pad11->BackColor = System::Drawing::SystemColors::Control;
			this->pad11->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad11->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad11->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad11->Location = System::Drawing::Point(174, 222);
			this->pad11->Name = L"pad11";
			this->pad11->Size = System::Drawing::Size(75, 75);
			this->pad11->Click += gcnew System::EventHandler(this, &mainForm::pad11_Click);
			// 
			// pad12
			// 
			this->pad12->BackColor = System::Drawing::SystemColors::Control;
			this->pad12->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad12->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad12->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad12->Location = System::Drawing::Point(255, 222);
			this->pad12->Name = L"pad12";
			this->pad12->Size = System::Drawing::Size(75, 75);
			this->pad12->Click += gcnew System::EventHandler(this, &mainForm::pad12_Click);
			// 
			// pad10
			// 
			this->pad10->BackColor = System::Drawing::SystemColors::Control;
			this->pad10->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad10->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad10->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad10->Location = System::Drawing::Point(93, 222);
			this->pad10->Name = L"pad10";
			this->pad10->Size = System::Drawing::Size(75, 75);
			this->pad10->Click += gcnew System::EventHandler(this, &mainForm::pad10_Click);
			// 
			// pad9
			// 
			this->pad9->BackColor = System::Drawing::SystemColors::Control;
			this->pad9->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->pad9->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->pad9->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->pad9->Location = System::Drawing::Point(12, 222);
			this->pad9->Name = L"pad9";
			this->pad9->Size = System::Drawing::Size(75, 75);
			this->pad9->Click += gcnew System::EventHandler(this, &mainForm::pad9_Click);
			// 
			// seq9
			// 
			this->seq9->BackColor = System::Drawing::SystemColors::Control;
			this->seq9->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq9->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq9->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq9->Location = System::Drawing::Point(174, 35);
			this->seq9->Name = L"seq9";
			this->seq9->Size = System::Drawing::Size(15, 15);
			// 
			// seq10
			// 
			this->seq10->BackColor = System::Drawing::SystemColors::Control;
			this->seq10->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq10->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq10->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq10->Location = System::Drawing::Point(194, 35);
			this->seq10->Name = L"seq10";
			this->seq10->Size = System::Drawing::Size(15, 15);
			// 
			// seq12
			// 
			this->seq12->BackColor = System::Drawing::SystemColors::Control;
			this->seq12->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq12->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq12->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq12->Location = System::Drawing::Point(234, 35);
			this->seq12->Name = L"seq12";
			this->seq12->Size = System::Drawing::Size(15, 15);
			// 
			// seq11
			// 
			this->seq11->BackColor = System::Drawing::SystemColors::Control;
			this->seq11->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq11->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq11->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq11->Location = System::Drawing::Point(214, 35);
			this->seq11->Name = L"seq11";
			this->seq11->Size = System::Drawing::Size(15, 15);
			// 
			// seq15
			// 
			this->seq15->BackColor = System::Drawing::SystemColors::Control;
			this->seq15->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq15->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq15->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq15->Location = System::Drawing::Point(294, 35);
			this->seq15->Name = L"seq15";
			this->seq15->Size = System::Drawing::Size(15, 15);
			// 
			// seq16
			// 
			this->seq16->BackColor = System::Drawing::SystemColors::Control;
			this->seq16->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq16->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq16->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq16->Location = System::Drawing::Point(315, 35);
			this->seq16->Name = L"seq16";
			this->seq16->Size = System::Drawing::Size(15, 15);
			// 
			// seq14
			// 
			this->seq14->BackColor = System::Drawing::SystemColors::Control;
			this->seq14->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq14->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq14->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq14->Location = System::Drawing::Point(274, 35);
			this->seq14->Name = L"seq14";
			this->seq14->Size = System::Drawing::Size(15, 15);
			// 
			// seq13
			// 
			this->seq13->BackColor = System::Drawing::SystemColors::Control;
			this->seq13->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq13->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq13->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq13->Location = System::Drawing::Point(254, 35);
			this->seq13->Name = L"seq13";
			this->seq13->Size = System::Drawing::Size(15, 15);
			// 
			// seq5
			// 
			this->seq5->BackColor = System::Drawing::SystemColors::Control;
			this->seq5->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq5->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq5->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq5->Location = System::Drawing::Point(93, 35);
			this->seq5->Name = L"seq5";
			this->seq5->Size = System::Drawing::Size(15, 15);
			// 
			// seq6
			// 
			this->seq6->BackColor = System::Drawing::SystemColors::Control;
			this->seq6->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq6->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq6->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq6->Location = System::Drawing::Point(113, 35);
			this->seq6->Name = L"seq6";
			this->seq6->Size = System::Drawing::Size(15, 15);
			// 
			// seq8
			// 
			this->seq8->BackColor = System::Drawing::SystemColors::Control;
			this->seq8->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq8->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq8->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq8->Location = System::Drawing::Point(153, 35);
			this->seq8->Name = L"seq8";
			this->seq8->Size = System::Drawing::Size(15, 15);
			// 
			// seq7
			// 
			this->seq7->BackColor = System::Drawing::SystemColors::Control;
			this->seq7->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq7->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq7->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq7->Location = System::Drawing::Point(133, 35);
			this->seq7->Name = L"seq7";
			this->seq7->Size = System::Drawing::Size(15, 15);
			// 
			// seq3
			// 
			this->seq3->BackColor = System::Drawing::SystemColors::Control;
			this->seq3->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq3->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq3->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq3->Location = System::Drawing::Point(53, 35);
			this->seq3->Name = L"seq3";
			this->seq3->Size = System::Drawing::Size(15, 15);
			// 
			// seq4
			// 
			this->seq4->BackColor = System::Drawing::SystemColors::Control;
			this->seq4->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq4->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq4->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq4->Location = System::Drawing::Point(73, 35);
			this->seq4->Name = L"seq4";
			this->seq4->Size = System::Drawing::Size(15, 15);
			// 
			// seq2
			// 
			this->seq2->BackColor = System::Drawing::SystemColors::Control;
			this->seq2->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq2->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq2->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq2->Location = System::Drawing::Point(33, 35);
			this->seq2->Name = L"seq2";
			this->seq2->Size = System::Drawing::Size(15, 15);
			// 
			// seq1
			// 
			this->seq1->BackColor = System::Drawing::SystemColors::Control;
			this->seq1->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->seq1->BorderColor = System::Drawing::SystemColors::ControlDark;
			this->seq1->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->seq1->Location = System::Drawing::Point(12, 35);
			this->seq1->Name = L"seq1";
			this->seq1->Size = System::Drawing::Size(15, 15);
			// 
			// tempoBar
			// 
			this->tempoBar->Location = System::Drawing::Point(340, 264);
			this->tempoBar->Maximum = 200;
			this->tempoBar->Minimum = 50;
			this->tempoBar->Name = L"tempoBar";
			this->tempoBar->Size = System::Drawing::Size(152, 45);
			this->tempoBar->TabIndex = 18;
			this->tempoBar->TickStyle = System::Windows::Forms::TickStyle::None;
			this->tempoBar->Value = 50;
			this->tempoBar->Scroll += gcnew System::EventHandler(this, &mainForm::tempoBar_Scroll);
			// 
			// lblTempo
			// 
			this->lblTempo->AutoSize = true;
			this->lblTempo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lblTempo->Location = System::Drawing::Point(350, 237);
			this->lblTempo->Name = L"lblTempo";
			this->lblTempo->Size = System::Drawing::Size(62, 20);
			this->lblTempo->TabIndex = 19;
			this->lblTempo->Text = L"Tempo:";
			// 
			// txtBoxTempo
			// 
			this->txtBoxTempo->BackColor = System::Drawing::SystemColors::MenuText;
			this->txtBoxTempo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->txtBoxTempo->ForeColor = System::Drawing::SystemColors::Window;
			this->txtBoxTempo->Location = System::Drawing::Point(418, 234);
			this->txtBoxTempo->Name = L"txtBoxTempo";
			this->txtBoxTempo->Size = System::Drawing::Size(66, 26);
			this->txtBoxTempo->TabIndex = 20;
			this->txtBoxTempo->Text = L"120";
			this->txtBoxTempo->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->txtBoxTempo->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &mainForm::txtBoxTempo_KeyDown);
			// 
			// btnPlay
			// 
			this->btnPlay->BackColor = System::Drawing::SystemColors::Control;
			this->btnPlay->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnPlay->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnPlay->Location = System::Drawing::Point(12, 383);
			this->btnPlay->Name = L"btnPlay";
			this->btnPlay->Size = System::Drawing::Size(76, 30);
			this->btnPlay->TabIndex = 21;
			this->btnPlay->Text = L"Play";
			this->btnPlay->UseVisualStyleBackColor = false;
			this->btnPlay->Click += gcnew System::EventHandler(this, &mainForm::btnPlay_Click);
			// 
			// btnPause
			// 
			this->btnPause->BackColor = System::Drawing::SystemColors::Control;
			this->btnPause->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnPause->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnPause->Location = System::Drawing::Point(93, 383);
			this->btnPause->Name = L"btnPause";
			this->btnPause->Size = System::Drawing::Size(76, 30);
			this->btnPause->TabIndex = 22;
			this->btnPause->Text = L"Pause";
			this->btnPause->UseVisualStyleBackColor = false;
			this->btnPause->Click += gcnew System::EventHandler(this, &mainForm::btnPause_Click);
			// 
			// btnStop
			// 
			this->btnStop->BackColor = System::Drawing::SystemColors::Control;
			this->btnStop->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnStop->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnStop->Location = System::Drawing::Point(174, 383);
			this->btnStop->Name = L"btnStop";
			this->btnStop->Size = System::Drawing::Size(76, 30);
			this->btnStop->TabIndex = 23;
			this->btnStop->Text = L"Stop";
			this->btnStop->UseVisualStyleBackColor = false;
			this->btnStop->Click += gcnew System::EventHandler(this, &mainForm::btnStop_Click);
			// 
			// timerTempo
			// 
			this->timerTempo->Tick += gcnew System::EventHandler(this, &mainForm::timerTempo_Tick);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(337, 35);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 15);
			this->label1->TabIndex = 24;
			// 
			// openFileDialogAudio
			// 
			this->openFileDialogAudio->FileName = L"openFileDialogAudio";
			// 
			// btnClear
			// 
			this->btnClear->BackColor = System::Drawing::SystemColors::Control;
			this->btnClear->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnClear->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnClear->Location = System::Drawing::Point(255, 383);
			this->btnClear->Name = L"btnClear";
			this->btnClear->Size = System::Drawing::Size(76, 30);
			this->btnClear->TabIndex = 26;
			this->btnClear->Text = L"Clear";
			this->btnClear->UseVisualStyleBackColor = false;
			this->btnClear->Click += gcnew System::EventHandler(this, &mainForm::btnRecord_Click);
			// 
			// btnSequence
			// 
			this->btnSequence->BackColor = System::Drawing::SystemColors::Control;
			this->btnSequence->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnSequence->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnSequence->Location = System::Drawing::Point(340, 383);
			this->btnSequence->Name = L"btnSequence";
			this->btnSequence->Size = System::Drawing::Size(94, 30);
			this->btnSequence->TabIndex = 27;
			this->btnSequence->Text = L"Sequence";
			this->btnSequence->UseVisualStyleBackColor = false;
			this->btnSequence->Click += gcnew System::EventHandler(this, &mainForm::btnSequence_Click);
			// 
			// btnSeqPrev
			// 
			this->btnSeqPrev->BackColor = System::Drawing::SystemColors::Control;
			this->btnSeqPrev->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnSeqPrev->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnSeqPrev->Location = System::Drawing::Point(462, 383);
			this->btnSeqPrev->Name = L"btnSeqPrev";
			this->btnSeqPrev->Size = System::Drawing::Size(30, 30);
			this->btnSeqPrev->TabIndex = 28;
			this->btnSeqPrev->Text = L"-";
			this->btnSeqPrev->UseVisualStyleBackColor = false;
			this->btnSeqPrev->Click += gcnew System::EventHandler(this, &mainForm::btnSeqPrev_Click);
			// 
			// btnSeqNext
			// 
			this->btnSeqNext->BackColor = System::Drawing::SystemColors::Control;
			this->btnSeqNext->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnSeqNext->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnSeqNext->Location = System::Drawing::Point(433, 383);
			this->btnSeqNext->Name = L"btnSeqNext";
			this->btnSeqNext->Size = System::Drawing::Size(30, 30);
			this->btnSeqNext->TabIndex = 29;
			this->btnSeqNext->Text = L"+";
			this->btnSeqNext->UseVisualStyleBackColor = false;
			this->btnSeqNext->Click += gcnew System::EventHandler(this, &mainForm::btnSeqNext_Click);
			// 
			// btnMet
			// 
			this->btnMet->BackColor = System::Drawing::SystemColors::Control;
			this->btnMet->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
			this->btnMet->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnMet->Location = System::Drawing::Point(538, 35);
			this->btnMet->Margin = System::Windows::Forms::Padding(0);
			this->btnMet->Name = L"btnMet";
			this->btnMet->Size = System::Drawing::Size(20, 20);
			this->btnMet->TabIndex = 30;
			this->btnMet->Text = L"M";
			this->btnMet->UseVisualStyleBackColor = false;
			this->btnMet->Click += gcnew System::EventHandler(this, &mainForm::btnMet_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(336, 60);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(47, 24);
			this->label2->TabIndex = 31;
			this->label2->Text = L"SLF";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(407, 63);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(145, 20);
			this->label3->TabIndex = 32;
			this->label3->Text = L"Sample Sequencer";
			// 
			// volumeBar
			// 
			this->volumeBar->Location = System::Drawing::Point(513, 253);
			this->volumeBar->Maximum = 100;
			this->volumeBar->Name = L"volumeBar";
			this->volumeBar->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->volumeBar->Size = System::Drawing::Size(45, 126);
			this->volumeBar->TabIndex = 33;
			this->volumeBar->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->volumeBar->Scroll += gcnew System::EventHandler(this, &mainForm::volumeBar_Scroll);
			// 
			// txtVolume
			// 
			this->txtVolume->BackColor = System::Drawing::SystemColors::MenuText;
			this->txtVolume->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->txtVolume->ForeColor = System::Drawing::SystemColors::Window;
			this->txtVolume->Location = System::Drawing::Point(511, 385);
			this->txtVolume->Name = L"txtVolume";
			this->txtVolume->Size = System::Drawing::Size(45, 26);
			this->txtVolume->TabIndex = 34;
			this->txtVolume->Text = L"80";
			this->txtVolume->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->txtVolume->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &mainForm::txtVolume_KeyDown);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(516, 237);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(36, 20);
			this->label4->TabIndex = 35;
			this->label4->Text = L"Vol.";
			// 
			// lboSample
			// 
			this->lboSample->BackColor = System::Drawing::SystemColors::MenuText;
			this->lboSample->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lboSample->ForeColor = System::Drawing::SystemColors::Window;
			this->lboSample->FormattingEnabled = true;
			this->lboSample->ItemHeight = 20;
			this->lboSample->Location = System::Drawing::Point(340, 96);
			this->lboSample->Name = L"lboSample";
			this->lboSample->Size = System::Drawing::Size(212, 104);
			this->lboSample->TabIndex = 36;
			// 
			// btnLoop4
			// 
			this->btnLoop4->BackColor = System::Drawing::SystemColors::Control;
			this->btnLoop4->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnLoop4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnLoop4->Location = System::Drawing::Point(340, 349);
			this->btnLoop4->Name = L"btnLoop4";
			this->btnLoop4->Size = System::Drawing::Size(39, 30);
			this->btnLoop4->TabIndex = 37;
			this->btnLoop4->Text = L"4";
			this->btnLoop4->UseVisualStyleBackColor = false;
			this->btnLoop4->Click += gcnew System::EventHandler(this, &mainForm::btnLoop4_Click);
			// 
			// btnLoop8
			// 
			this->btnLoop8->BackColor = System::Drawing::SystemColors::Control;
			this->btnLoop8->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnLoop8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnLoop8->Location = System::Drawing::Point(378, 349);
			this->btnLoop8->Name = L"btnLoop8";
			this->btnLoop8->Size = System::Drawing::Size(39, 30);
			this->btnLoop8->TabIndex = 38;
			this->btnLoop8->Text = L"8";
			this->btnLoop8->UseVisualStyleBackColor = false;
			this->btnLoop8->Click += gcnew System::EventHandler(this, &mainForm::btnLoop8_Click);
			// 
			// btnLoop16
			// 
			this->btnLoop16->BackColor = System::Drawing::SystemColors::Control;
			this->btnLoop16->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnLoop16->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnLoop16->Location = System::Drawing::Point(454, 349);
			this->btnLoop16->Name = L"btnLoop16";
			this->btnLoop16->Size = System::Drawing::Size(39, 30);
			this->btnLoop16->TabIndex = 40;
			this->btnLoop16->Text = L"16";
			this->btnLoop16->UseVisualStyleBackColor = false;
			this->btnLoop16->Click += gcnew System::EventHandler(this, &mainForm::btnLoop16_Click);
			// 
			// btnLoop12
			// 
			this->btnLoop12->BackColor = System::Drawing::SystemColors::Control;
			this->btnLoop12->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->btnLoop12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btnLoop12->Location = System::Drawing::Point(416, 349);
			this->btnLoop12->Name = L"btnLoop12";
			this->btnLoop12->Size = System::Drawing::Size(39, 30);
			this->btnLoop12->TabIndex = 39;
			this->btnLoop12->Text = L"12";
			this->btnLoop12->UseVisualStyleBackColor = false;
			this->btnLoop12->Click += gcnew System::EventHandler(this, &mainForm::btnLoop12_Click);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(336, 326);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(103, 20);
			this->label5->TabIndex = 41;
			this->label5->Text = L"Loop Length:";
			// 
			// mainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ActiveBorder;
			this->ClientSize = System::Drawing::Size(570, 426);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->btnLoop16);
			this->Controls->Add(this->btnLoop12);
			this->Controls->Add(this->btnLoop8);
			this->Controls->Add(this->btnLoop4);
			this->Controls->Add(this->lboSample);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->txtVolume);
			this->Controls->Add(this->volumeBar);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->btnMet);
			this->Controls->Add(this->btnSeqNext);
			this->Controls->Add(this->btnSeqPrev);
			this->Controls->Add(this->btnSequence);
			this->Controls->Add(this->btnClear);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->btnStop);
			this->Controls->Add(this->btnPause);
			this->Controls->Add(this->btnPlay);
			this->Controls->Add(this->txtBoxTempo);
			this->Controls->Add(this->lblTempo);
			this->Controls->Add(this->tempoBar);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->shapeContainer1);
			this->Cursor = System::Windows::Forms::Cursors::Hand;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"mainForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"SLF   Sample Sequencer";
			this->Load += gcnew System::EventHandler(this, &mainForm::mainForm_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->tempoBar))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->volumeBar))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

private: System::Void setLoopLength(int Length)
		 {
			 switch(Length)
			 {
			 case 4:
				 btnLoop4->BackColor = System::Drawing::SystemColors::Info;
				 btnLoop8->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop12->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop16->BackColor = System::Drawing::SystemColors::Control;
				 break;
			 case 8:
				 btnLoop4->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop8->BackColor = System::Drawing::SystemColors::Info;
				 btnLoop12->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop16->BackColor = System::Drawing::SystemColors::Control;
				 break;
			 case 12:
				 btnLoop4->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop8->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop12->BackColor = System::Drawing::SystemColors::Info;
				 btnLoop16->BackColor = System::Drawing::SystemColors::Control;
				 break;
			 case 16:
				 btnLoop4->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop8->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop12->BackColor = System::Drawing::SystemColors::Control;
				 btnLoop16->BackColor = System::Drawing::SystemColors::Info;
				 break;
			 }
		 }
private: System::Void resetPadColour()
		 {
			 pad1->BackColor = System::Drawing::SystemColors::Control;
			 pad2->BackColor = System::Drawing::SystemColors::Control;
			 pad3->BackColor = System::Drawing::SystemColors::Control;
			 pad4->BackColor = System::Drawing::SystemColors::Control;
			 pad5->BackColor = System::Drawing::SystemColors::Control;
			 pad6->BackColor = System::Drawing::SystemColors::Control;
			 pad7->BackColor = System::Drawing::SystemColors::Control;
			 pad8->BackColor = System::Drawing::SystemColors::Control;
			 pad9->BackColor = System::Drawing::SystemColors::Control;
			 pad10->BackColor = System::Drawing::SystemColors::Control;
			 pad11->BackColor = System::Drawing::SystemColors::Control;
			 pad12->BackColor = System::Drawing::SystemColors::Control;
			 pad13->BackColor = System::Drawing::SystemColors::Control;
			 pad14->BackColor = System::Drawing::SystemColors::Control;
			 pad15->BackColor = System::Drawing::SystemColors::Control;
			 pad16->BackColor = System::Drawing::SystemColors::Control;
		 }
private: System::Void padSequence(int pad)
		 {
			 String ^hitYes, ^hitNo, ^hitGap;
			 hitYes = "+";
			 hitNo = "-";
			 hitGap = "|";
			 hits = "";
			 resetPadColour();
			 switch(pad)
			 {
			 case 1:
				 if(playPad1[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad1[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad1[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad1[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad1[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad1[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad1[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad1[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad1[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad1[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad1[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad1[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad1[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad1[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad1[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad1[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad1->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 2:
				if(playPad2[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad2[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad2[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad2[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad2[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad2[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad2[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad2[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad2[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad2[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad2[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad2[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad2[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad2[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad2[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad2[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad2->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 3:
				 if(playPad3[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad3[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad3[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad3[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad3[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad3[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad3[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad3[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad3[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad3[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad3[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad3[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad3[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad3[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad3[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad3[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad3->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 4:
				 if(playPad4[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad4[2])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad4[3])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad4[4])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad4[5])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad4[6])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad4[7])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad4[8])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad4[9])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad4[10])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad4[11])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad4[12])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad4[13])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad4[14])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad4[15])
				 {
					 hits = hits + hitYes + hitGap;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad4[16])
				 {
					 hits = hits + hitYes;
					  if(seqMode)
					 {
						 pad4->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 5:
				 if(playPad5[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad5[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad5[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad5[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad5[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad5[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad5[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad5[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad5[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad5->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad6[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad6->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad6[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad6->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad6[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad6->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad6[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad6->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad6[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad6->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad6[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad6->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad6[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad6->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 7:
				 if(playPad7[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad7[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad7[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad7[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad7[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad7[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad7[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad7[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad7[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad7[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad7[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad7[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad7[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad7[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad7[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad7[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad7->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 8:
				 if(playPad8[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad8[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad8[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad8[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad8[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad8[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad8[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad8[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad8[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad8[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad8[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad8[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad8[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad8[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad8[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad8[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad8->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 9:
				 if(playPad9[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad9[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad9[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad9[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad9[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad9[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad9[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad9[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad9[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad9[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad9[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad9[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad9[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad9[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad9[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad9[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad9->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 10:
				 if(playPad10[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad10[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad10[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad10[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad10[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad10[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad10[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad10[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad10[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad10[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad10[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad10[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad10[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad10[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad10[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad10[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad10->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 11:
				 if(playPad11[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad11[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad11[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad11[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad11[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad11[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad11[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad11[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad11[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad11[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad11[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad11[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad11[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad11[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad11[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad11[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad11->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 12:
				 if(playPad12[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad12[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad12[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad12[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad12[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad12[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad12[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad12[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad12[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad12[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad12[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad12[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad12[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad12[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad12[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad12[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad12->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 13:
				 if(playPad13[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad13[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad13[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad13[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad13[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad13[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad13[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad13[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad13[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad13[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad13[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad13[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad13[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad13[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad13[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad13[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad13->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 14:
				 if(playPad14[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad14[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad14[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad14[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad14[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad14[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad14[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad14[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad14[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad14[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad14[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad14[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad14[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad14[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad14[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad14[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad14->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 15:
				 if(playPad15[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad15[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad15[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad15[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad15[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad15[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad15[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad15[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad15[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad15[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad15[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad15[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad15[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad15[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad15[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad15[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad15->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			case 16:
				 if(playPad16[1])
				 {
					 hits = hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hitNo + hitGap;
				 }
				 if(playPad16[2])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad16[3])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad16[4])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad16[5])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad16[6])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad16[7])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					 if(playPad16[8])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					  if(playPad16[9])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					   if(playPad16[10])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
					    if(playPad16[11])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
						 if(playPad16[12])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				 if(playPad16[13])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				  if(playPad16[14])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				   if(playPad16[15])
				 {
					 hits = hits + hitYes + hitGap;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo + hitGap;
				 }
				    if(playPad16[16])
				 {
					 hits = hits + hitYes;
					 if(seqMode)
					 {
						 pad16->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
					 }
				 }
				 else
				 {
					 hits = hits + hitNo;
				 }
				break;
			}
		 }
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
			 {
				 Application::Exit();
			 }
private: System::Void mainForm_Load(System::Object^  sender, System::EventArgs^  e)
		 {
			 Tempo = int::Parse(txtBoxTempo->Text);
			 tempoBar->Value = Tempo;
			 Volume = int::Parse(txtVolume->Text);
			 volumeBar->Value = Volume;
		 }
private: System::Void tempoBar_Scroll(System::Object^  sender, System::EventArgs^  e)
		 {
			 Tempo = tempoBar->Value;
			 txtBoxTempo->Text = Tempo.ToString();
		 }
private: System::Void txtBoxTempo_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
		 {
			 if (e->KeyCode == Keys::Enter)
			 {
				 Tempo = int::Parse(txtBoxTempo->Text);
				 tempoBar->Value = Tempo;
			 }
		 }
private: System::Void btnPlay_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 //Set timer
			 timerTempo->Enabled = true;
			 timerInterval = (int)(60000/(Tempo*4));
			 timerTempo->Interval = timerInterval;
			 timerTempo->Start();
			 
			 //Initalise Audio
			 audioOn = true;
		 }
private: System::Void btnPause_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 //Pause play-back of user defined sequence
			 timerTempo->Stop();
			 //audio-on variable remains set to true
		 }
private: System::Void btnStop_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 //Stop play-back of user defined sequence
			 timerTempo->Stop();
			 //Return to the start of the sequence
			 
			 audioOn = false;
			 resetSequencer();
		 }
private: System::Void btnRecord_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 //Clear samples from pads
			 audioFilePad1 = "";
			 audioFilePad2 = "";
			 audioFilePad3 = "";
			 audioFilePad4 = "";
			 audioFilePad5 = "";
			 audioFilePad6 = "";
			 audioFilePad7 = "";
			 audioFilePad8 = "";
			 audioFilePad9 = "";
			 audioFilePad10 = "";
			 audioFilePad11 = "";
			 audioFilePad12 = "";
			 audioFilePad13 = "";
			 audioFilePad14 = "";
			 audioFilePad15 = "";
			 audioFilePad16 = "";
			 lboSample->Items[1] = "";
		 }
private: System::Void pad1_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad1))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad1 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad1Player = gcnew System::Media::SoundPlayer(audioFilePad1);
					 pad1Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad1))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad1 = openFileDialogAudio->FileName;
				 }
			 }

			 if(seqEdit1)
			 {
				 playPad1[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad1[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad1[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad1[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad1[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad1[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad1[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad1[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad1[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad1[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad1[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad1[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad1[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad1[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad1[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad1[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad1");
			 sampleName = audioFilePad1->Substring(audioFilePad1->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(1);
			 this->lboSample->Items->Add(hits);

		 }
private: System::Void pad2_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad2))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad2 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad2Player = gcnew System::Media::SoundPlayer(audioFilePad2);
					 pad2Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad2))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad2 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad2[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad2[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad2[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad2[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad2[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad2[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad2[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad2[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad2[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad2[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad2[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad2[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad2[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad2[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad2[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad2[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad2");
			 sampleName = audioFilePad2->Substring(audioFilePad2->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(2);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad3_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad3))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad3 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad3Player = gcnew System::Media::SoundPlayer(audioFilePad3);
					 pad3Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad3))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad3 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad3[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad3[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad3[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad3[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad3[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad3[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad3[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad3[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad3[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad3[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad3[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad3[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad3[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad3[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad3[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad3[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad3");
			 sampleName = audioFilePad3->Substring(audioFilePad3->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(3);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad4_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad4))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad4 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad4Player = gcnew System::Media::SoundPlayer(audioFilePad4);
					 pad4Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad4))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad4 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad4[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad4[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad4[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad4[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad4[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad4[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad4[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad4[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad4[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad4[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad4[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad4[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad4[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad4[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad4[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad4[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad4");
			 sampleName = audioFilePad4->Substring(audioFilePad4->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(4);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad5_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad5))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad5 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad5Player = gcnew System::Media::SoundPlayer(audioFilePad5);
					 pad5Player->Play();
				 }
			 }
			 else
			 {
				if(String::IsNullOrEmpty(audioFilePad5))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad5 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad5[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad5[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad5[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad5[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad5[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad5[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad5[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad5[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad5[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad5[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad5[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad5[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad5[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad5[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad5[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad5[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad5");
			 sampleName = audioFilePad5->Substring(audioFilePad5->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(5);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad6_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad6))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad6 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad6Player = gcnew System::Media::SoundPlayer(audioFilePad6);
					 pad6Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad6))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad6 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad6[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad6[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad6[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad6[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad6[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad6[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad6[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad6[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad6[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad6[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad6[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad6[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad6[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad6[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad6[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad6[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad6");
			 sampleName = audioFilePad6->Substring(audioFilePad6->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(6);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad7_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad7))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad7 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad7Player = gcnew System::Media::SoundPlayer(audioFilePad7);
					 pad7Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad7))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad7 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad7[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad7[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad7[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad7[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad7[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad7[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad7[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad7[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad7[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad7[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad7[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad7[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad7[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad7[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad7[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad7[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad7");
			 sampleName = audioFilePad7->Substring(audioFilePad7->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(7);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad8_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad8))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad8 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad8Player = gcnew System::Media::SoundPlayer(audioFilePad8);
					 pad8Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad8))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad8 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad8[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad8[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad8[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad8[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad8[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad8[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad8[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad8[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad8[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad8[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad8[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad8[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad8[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad8[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad8[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad8[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad8");
			 sampleName = audioFilePad8->Substring(audioFilePad8->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(8);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad9_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad9))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad9 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad9Player = gcnew System::Media::SoundPlayer(audioFilePad9);
					 pad9Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad9))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad9 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad9[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad9[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad9[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad9[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad9[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad9[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad9[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad9[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad9[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad9[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad9[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad9[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad9[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad9[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad9[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad9[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad9");
			 sampleName = audioFilePad9->Substring(audioFilePad9->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(9);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad10_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad10))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad10 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad10Player = gcnew System::Media::SoundPlayer(audioFilePad10);
					 pad10Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad10))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad10 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad10[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad10[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad10[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad10[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad10[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad10[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad10[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad10[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad10[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad10[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad10[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad10[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad10[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad10[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad10[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad10[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad10");
			 sampleName = audioFilePad10->Substring(audioFilePad10->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(10);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad11_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad11))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad11 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad11Player = gcnew System::Media::SoundPlayer(audioFilePad11);
					 pad11Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad11))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad11 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad11[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad11[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad11[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad11[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad11[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad11[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad11[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad11[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad11[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad11[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad11[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad11[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad11[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad11[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad11[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad11[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad11");
			 sampleName = audioFilePad11->Substring(audioFilePad11->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(11);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad12_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad12))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad12 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad12Player = gcnew System::Media::SoundPlayer(audioFilePad12);
					 pad12Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad12))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad12 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad12[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad12[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad12[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad12[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad12[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad12[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad12[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad12[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad12[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad12[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad12[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad12[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad12[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad12[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad12[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad12[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad12");
			 sampleName = audioFilePad12->Substring(audioFilePad12->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(12);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad13_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad13))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad13 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad13Player = gcnew System::Media::SoundPlayer(audioFilePad13);
					 pad13Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad13))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad13 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad13[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad13[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad13[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad13[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad13[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad13[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad13[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad13[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad13[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad13[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad13[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad13[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad13[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad13[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad13[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad13[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad13");
			 sampleName = audioFilePad13->Substring(audioFilePad13->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(13);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad14_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad14))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad14 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad14Player = gcnew System::Media::SoundPlayer(audioFilePad14);
					 pad14Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad14))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad14 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad14[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad14[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad14[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad14[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad14[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad14[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad14[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad14[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad14[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad14[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad14[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad14[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad14[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad14[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad14[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad14[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad14");
			 sampleName = audioFilePad14->Substring(audioFilePad14->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(14);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad15_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad15))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad15 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad15Player = gcnew System::Media::SoundPlayer(audioFilePad15);
					 pad15Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad15))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad15 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad15[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad15[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad15[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad15[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad15[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad15[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad15[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad15[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad15[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad15[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad15[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad15[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad15[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad15[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad15[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad15[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad15");
			 sampleName = audioFilePad15->Substring(audioFilePad15->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(15);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void pad16_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(audioOn)
			 {
				 if(String::IsNullOrEmpty(audioFilePad16))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad16 = openFileDialogAudio->FileName;
				 }
				 else
				 {
					 //Play Audio
					 pad16Player = gcnew System::Media::SoundPlayer(audioFilePad16);
					 pad16Player->Play();
				 }
			 }
			 else
			 {
				 if(String::IsNullOrEmpty(audioFilePad16))
				 {
					 //Get audio file
					 OpenFileDialog^ openFileDialogAudio = gcnew OpenFileDialog;
					 openFileDialogAudio->ShowDialog();
					 audioFilePad16 = openFileDialogAudio->FileName;
				 }
			 }
			 if(seqEdit1)
			 {
				 playPad16[1] = true;
			 }
			 if(seqEdit2)
			 {
				 playPad16[2] = true;
			 }
			 if(seqEdit3)
			 {
				 playPad16[3] = true;
			 }
			 if(seqEdit4)
			 {
				 playPad16[4] = true;
			 }
			 if(seqEdit5)
			 {
				 playPad16[5] = true;
			 }
			 if(seqEdit6)
			 {
				 playPad16[6] = true;
			 }
			 if(seqEdit7)
			 {
				 playPad16[7] = true;
			 }
			 if(seqEdit8)
			 {
				 playPad16[8] = true;
			 }
			 if(seqEdit9)
			 {
				 playPad16[9] = true;
			 }
			 if(seqEdit10)
			 {
				 playPad16[10] = true;
			 }
			 if(seqEdit11)
			 {
				 playPad16[11] = true;
			 }
			 if(seqEdit12)
			 {
				 playPad16[12] = true;
			 }
			 if(seqEdit13)
			 {
				 playPad16[13] = true;
			 }
			 if(seqEdit14)
			 {
				 playPad16[14] = true;
			 }
			 if(seqEdit15)
			 {
				 playPad16[15] = true;
			 }
			 if(seqEdit16)
			 {
				 playPad16[16] = true;
			 }
			 this->lboSample->Items->Clear();
			 this->lboSample->Items->Add("Pad16");
			 sampleName = audioFilePad16->Substring(audioFilePad16->Length - 20);
			 sampleName = dots + sampleName;
			 this->lboSample->Items->Add(sampleName);
			 this->lboSample->Items->Add("");
			 padSequence(16);
			 this->lboSample->Items->Add(hits);
		 }
private: System::Void playSequence(int position)
		 {
			 switch (position)
			 {
			 case 1:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 2:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			case 3:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 4:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 5:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 6:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 7:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 8:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 9:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 10:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 11:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 12:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 13:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 14:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 15:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 case 16:
				 if(playPad1[position])
				 {
					 //Play Audio
					 pad1Player->Play();
				 }
				 if(playPad2[position])
				 {
					 //Play Audio
					 pad2Player->Play();
				 }
				 if(playPad3[position])
				 {
					 //Play Audio
					 pad3Player->Play();
				 }
				 if(playPad4[position])
				 {
					 //Play Audio
					 pad4Player->Play();
				 }
				 if(playPad5[position])
				 {
					 //Play Audio
					 pad5Player->Play();
				 }
				 if(playPad6[position])
				 {
					 //Play Audio
					 pad6Player->Play();
				 }
				 if(playPad7[position])
				 {
					 //Play Audio
					 pad7Player->Play();
				 }
				 if(playPad8[position])
				 {
					 //Play Audio
					 pad8Player->Play();
				 }
				 if(playPad9[position])
				 {
					 //Play Audio
					 pad9Player->Play();
				 }
				 if(playPad10[position])
				 {
					 //Play Audio
					 pad10Player->Play();
				 }
				 if(playPad11[position])
				 {
					 //Play Audio
					 pad11Player->Play();
				 }
				 if(playPad12[position])
				 {
					 //Play Audio
					 pad12Player->Play();
				 }
				 if(playPad13[position])
				 {
					 //Play Audio
					 pad13Player->Play();
				 }
				 if(playPad14[position])
				 {
					 //Play Audio
					 pad14Player->Play();
				 }
				 if(playPad15[position])
				 {
					 //Play Audio
					 pad15Player->Play();
				 }
				 if(playPad16[position])
				 {
					 //Play Audio
					 pad16Player->Play();
				 }
				 break;
			 }
		 }
private: System::Void timerTempo_Tick(System::Object^  sender, System::EventArgs^  e) 
		 {
			 //check sequence position variable, so that the timer loops through a 16 beat loop
			 if(seqPosition > LoopLength)
			 {
				 seqPosition = 1;
			 }

			 //check sequence position for metronome
			 if(metronome)
			 {
				 if(seqPosition == 1 || seqPosition == 5 || seqPosition == 9 || seqPosition == 13)
				 {
					 metronomePlayer->Play();
				 }
			 }

			 //Check whereabouts in the loop the timer is, perform relevant task
			 switch (seqPosition)
			 {
			 case 1:
				 resetSequencerLights();
				 seq1->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 2:
				 resetSequencerLights();
				 seq2->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 3:
				 resetSequencerLights();
				 seq3->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 4:
				 resetSequencerLights();
				 seq4->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 5:
				 resetSequencerLights();
				 seq5->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 6:
				 resetSequencerLights();
				 seq6->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 7:
				 resetSequencerLights();
				 seq7->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 8:
				 resetSequencerLights();
				 seq8->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 9:
				 resetSequencerLights();
				 seq9->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 10:
				 resetSequencerLights();
				 seq10->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 11:
				 resetSequencerLights();
				 seq11->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 12:
				 resetSequencerLights();
				 seq12->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 13:
				 resetSequencerLights();
				 seq13->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 14:
				 resetSequencerLights();
				 seq14->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 15:
				 resetSequencerLights();
				 seq15->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 case 16:
				 resetSequencerLights();
				 seq16->BackColor = System::Drawing::SystemColors::Info;
				 playSequence(seqPosition);
				 break;
			 }

			 label1->Text = Convert::ToString(seqPosition);
			 seqPosition++;
		 }

private: System::Void resetSequencerLights()
		 {
			seq1->BackColor = System::Drawing::SystemColors::Control;
			seq2->BackColor = System::Drawing::SystemColors::Control;
			seq3->BackColor = System::Drawing::SystemColors::Control;
			seq4->BackColor = System::Drawing::SystemColors::Control;
			seq5->BackColor = System::Drawing::SystemColors::Control;
			seq6->BackColor = System::Drawing::SystemColors::Control;
			seq7->BackColor = System::Drawing::SystemColors::Control;
			seq8->BackColor = System::Drawing::SystemColors::Control;
			seq9->BackColor = System::Drawing::SystemColors::Control;
			seq10->BackColor = System::Drawing::SystemColors::Control;
			seq11->BackColor = System::Drawing::SystemColors::Control;
			seq12->BackColor = System::Drawing::SystemColors::Control;
			seq13->BackColor = System::Drawing::SystemColors::Control;
			seq14->BackColor = System::Drawing::SystemColors::Control;
			seq15->BackColor = System::Drawing::SystemColors::Control;
			seq16->BackColor = System::Drawing::SystemColors::Control;
		 }
private: System::Void resetSequencer()
		{
			resetSequencerLights();
			seq1->BackColor = System::Drawing::SystemColors::Info;
			seqPosition = 1;
			label1->Text = Convert::ToString(seqPosition);
		}
private: System::Void setMetronome()
		 {
			 if(metronome)
			 {
				 metronome = false;
				 btnMet->BackColor = System::Drawing::SystemColors::Control;
			 }
			 else
			 {
				 metronome = true;
				 btnMet->BackColor = System::Drawing::SystemColors::Info;
			 }
		 }
private: System::Void btnMet_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 setMetronome();
		 }
private: System::Void findEditPos(int position)
		 {
			 switch(position)
			 {
			 case 1:
				 seqEdit1 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 2:
				 seqEdit2 = true;
				 seqEdit1 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 3:
				 seqEdit3 = true;
				 seqEdit2 = seqEdit1 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 4:
				 seqEdit4 = true;
				 seqEdit2 = seqEdit3 = seqEdit1 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 5:
				 seqEdit5 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit1 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 6:
				 seqEdit6 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit1 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 7:
				 seqEdit7 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit1 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 8:
				 seqEdit8 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit1 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 9:
				 seqEdit9 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit1 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 10:
				 seqEdit10 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit1 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 11:
				 seqEdit11 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit1 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 12:
				 seqEdit12 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit1 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 13:
				 seqEdit13 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit1 = seqEdit14 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 14:
				 seqEdit14 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit1 = seqEdit15 = seqEdit16 = false;
				 break;
			 case 15:
				 seqEdit15 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit1 = seqEdit16 = false;
				 break;
			 case 16:
				 seqEdit16 = true;
				 seqEdit2 = seqEdit3 = seqEdit4 = seqEdit5 = seqEdit6 = seqEdit7 = seqEdit8 = seqEdit9 = false; 
				 seqEdit10 = seqEdit11 = seqEdit12 = seqEdit13 = seqEdit14 = seqEdit15 = seqEdit1 = false;
				 break;
			 }
		 }
private: System::Void editSequence(bool seqOn)
		 {
			 if(seqOn)
			 {
				 switch (seqPosition)
				 {
				 case 1:
					 findEditPos(1);
					 resetSequencerLights();
					 seq1->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 2:
					 findEditPos(2);
					 resetSequencerLights();
					 seq2->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 3:
					 findEditPos(3);
					 resetSequencerLights();
					 seq3->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 4:
					 findEditPos(4);
					 resetSequencerLights();
					 seq4->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 5:
					 findEditPos(5);
					 resetSequencerLights();
					 seq5->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 6:
					 findEditPos(6);
					 resetSequencerLights();
					 seq6->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 7:
					 findEditPos(7);
					 resetSequencerLights();
					 seq7->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 8:
					 findEditPos(8);
					 resetSequencerLights();
					 seq8->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 9:
					 findEditPos(9);
					 resetSequencerLights();
					 seq9->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 10:
					 findEditPos(10);
					 resetSequencerLights();
					 seq10->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 11:
					 findEditPos(11);
					 resetSequencerLights();
					 seq11->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 12:
					 findEditPos(12);
					 resetSequencerLights();
					 seq12->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 13:
					 findEditPos(13);
					 resetSequencerLights();
					 seq13->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 14:
					 findEditPos(14);
					 resetSequencerLights();
					 seq14->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 15:
					 findEditPos(15);
					 resetSequencerLights();
					 seq15->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 case 16:
					 findEditPos(16);
					 resetSequencerLights();
					 seq16->BackColor = System::Drawing::SystemColors::Info;
					 break;
				 }
			 }
		 }
private: System::Void setSequenceMode()
		 {
			 if(seqMode)
			 {
				 seqMode = false;
				 btnSequence->BackColor = System::Drawing::SystemColors::Control;
			 }
			 else
			 {
				 seqMode = true;
				 btnSequence->BackColor = System::Drawing::SystemColors::Info;
			 }
			 seqPosition = 1;
			 label1->Text = Convert::ToString(seqPosition);
			 resetSequencer();
			 editSequence(seqMode);
		 }
private: System::Void btnSequence_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 setSequenceMode();
			 padSequence(seqPosition);
		 }
private: System::Void btnSeqNext_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 seqPosition++;
			 if(seqPosition == 17)
			 {
				 seqPosition = 1;
			 }
			 label1->Text = Convert::ToString(seqPosition);
			 editSequence(seqMode);
			 padSequence(seqPosition);
		 }
private: System::Void btnSeqPrev_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 seqPosition--;
			 if(seqPosition == 0)
			 {
				 seqPosition = 16;
			 }
			 label1->Text = Convert::ToString(seqPosition);
			 editSequence(seqMode);
			 padSequence(seqPosition);
		 }
private: System::Void metronomeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 setMetronome();
		 }
private: System::Void sequenceModeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 setSequenceMode();
		 }
private: System::Void btnLoop4_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 LoopLength = 4;
			 setLoopLength(LoopLength);
			 resetSequencerLights();
		 }
private: System::Void btnLoop8_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 LoopLength = 8;
			 setLoopLength(LoopLength);
			 resetSequencerLights();
		 }
private: System::Void btnLoop12_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 LoopLength = 12;
			 setLoopLength(LoopLength);
			 resetSequencerLights();
		 }
private: System::Void btnLoop16_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 LoopLength = 16;
			 setLoopLength(LoopLength);
			 resetSequencerLights();
		 }
private: System::Void volumeBar_Scroll(System::Object^  sender, System::EventArgs^  e) 
		 {
			 Volume = volumeBar->Value;
			 txtVolume->Text = Volume.ToString();
		 }
private: System::Void txtVolume_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
		 {
			 if (e->KeyCode == Keys::Enter)
			 {
				 Volume = int::Parse(txtVolume->Text);
				 volumeBar->Value = Volume;
			 }
		 }
};
}
